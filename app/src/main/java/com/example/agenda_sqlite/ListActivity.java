package com.example.agenda_sqlite;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import database.AgendaContactos;
import database.Contacto;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class ListActivity extends android.app.ListActivity {
    private AgendaContactos agendaContactos;
    private Button btnNuevo;
    private ArrayList<Contacto> listaContacto;
    private MyArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        btnNuevo = (Button) findViewById(R.id.btnNuevo);
        agendaContactos = new AgendaContactos(this);

        llenarLista();
        adapter = new MyArrayAdapter(this, R.layout.layout_contacto, listaContacto);
        String str = adapter.contactos.get(1).getNombre();
        setListAdapter(adapter);

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void llenarLista(){
        agendaContactos.openDatabase();
        listaContacto = agendaContactos.allContactos();
        agendaContactos.cerrar();
    }

    //clase Anidada dentro del ListActivity
    class MyArrayAdapter extends ArrayAdapter<Contacto>{
        private Context context;
        private int textViewResourceId;
        private ArrayList<Contacto> contactos;

        public MyArrayAdapter(@NonNull Context context, int resource, ArrayList<Contacto> contactos){
            super(context, resource);
            this.context = context;
            this.textViewResourceId = resource;
            this.contactos = contactos;
        }

        @NonNull
        public View getView(final int position, View convertView, @NonNull ViewGroup viewGroup){
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert layoutInflater != null;
            @SuppressLint("ViewHolder") View view = layoutInflater.inflate(this.textViewResourceId, null);
            //debido a diferencia en nombre, se requiere cambiar las siguientes lineas
            TextView lblNombreContacto = (TextView)view.findViewById(R.id.lblNombreContacto);
            TextView lblTelefonoContacto = (TextView)view.findViewById(R.id.lblTelefonoContacto);

            Button btnModificar = (Button)view.findViewById(R.id.btnModificar);
            Button btnBorrar = (Button)view.findViewById(R.id.btnBorrar);

            if(contactos.get(position).getFavorito()>0){
                lblNombreContacto.setTextColor((Color.BLUE));
                lblTelefonoContacto.setTextColor(Color.BLUE);
            }else{
                lblNombreContacto.setTextColor((Color.BLACK));
                lblTelefonoContacto.setTextColor(Color.BLACK);

            }
            lblNombreContacto.setText(contactos.get(position).getNombre());
            lblTelefonoContacto.setText(contactos.get(position).getTelefono1());

            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    agendaContactos.openDatabase();
                    agendaContactos.deleteContacto(contactos.get(position).get_ID());
                    agendaContactos.cerrar();
                    contactos.remove(position);
                    notifyDataSetChanged();
                }
            });

            btnModificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("contacto",contactos.get(position));
                    Intent i = new Intent();
                    i.putExtras(bundle);
                    //Se agrega el objeto intent para enviar la información al mainActivity
                    setResult(Activity.RESULT_OK, i);
                }
            });
            return view;
        }
    }


}
