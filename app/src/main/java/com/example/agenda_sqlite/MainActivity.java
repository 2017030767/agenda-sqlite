package com.example.agenda_sqlite;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import database.AgendaContactos;

import database.Contacto;

import android.app.Activity;
import android.content.Intent;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView lblNombre;
    private EditText txtNombre;
    private TextView lblTelefono1;
    private EditText txtTelefono1;
    private TextView lblTelefono2;
    private EditText txtTelefono2;
    private TextView lblDomicilio;
    private EditText txtDomicilio;
    private TextView lblNotas;
    private EditText txtNotas;
    private CheckBox checkBoxFav;
    private Button btnLimpiar;
    private Button btnGuardar;
    private Button btnListar;
    private AgendaContactos db;
    private Contacto savedContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lblNombre = (TextView) findViewById(R.id.lblNombre);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        lblTelefono1 = (TextView) findViewById(R.id.lbltel1);
        txtTelefono1 = (EditText) findViewById(R.id.txtTel1);
        lblTelefono2 = (TextView) findViewById(R.id.lbltel2);
        txtTelefono2 = (EditText) findViewById(R.id.txtTel2);
        lblDomicilio = (TextView) findViewById(R.id.lblDir);
        txtDomicilio = (EditText) findViewById(R.id.txtDomicilio);
        lblNotas = (TextView) findViewById(R.id.lblNota);
        txtNotas = (EditText) findViewById(R.id.txtNota);
        checkBoxFav = (CheckBox) findViewById(R.id.chkFavorito);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnListar = (Button) findViewById(R.id.btnListar);

        db = new AgendaContactos(MainActivity.this);

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiar();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txtNombre.getText().toString().equals("") ||
                        txtTelefono1.getText().toString().equals("") ||
                        txtDomicilio.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, R.string.msgerror, Toast.LENGTH_SHORT).show();
                }else{
                    Contacto c = new Contacto(0,
                            txtNombre.getText().toString(),
                            txtTelefono1.getText().toString(),
                            txtTelefono2.getText().toString(),
                            txtDomicilio.getText().toString(),
                            txtNotas.getText().toString(),
                            checkBoxFav.isChecked()?1:0);
                    db.openDatabase();

                    if(savedContact != null){
                        long idx = db.updateContacto(c, savedContact.get_ID());
                        Toast.makeText(MainActivity.this, "Se actualizo el registro: "+ idx, Toast.LENGTH_SHORT).show();
                        savedContact = null;
                    }else{
                        long idx = db.insertarContacto(c);
                        Toast.makeText(MainActivity.this, "Se agrego contacto con ID: "+ idx, Toast.LENGTH_SHORT).show();
                    }
                    db.cerrar();
                    limpiar();

                }

            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,
                        ListActivity.class);
                startActivityForResult(i,0);
            }
        });

    }


    public void limpiar(){
        txtNombre.setText("");
        txtTelefono1.setText("");
        txtTelefono2.setText("");
        txtDomicilio.setText("");
        txtNotas.setText("");
        checkBoxFav.setChecked(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(Activity.RESULT_OK == resultCode){
            Contacto contacto = (Contacto) data.getSerializableExtra("contacto");
            savedContact = contacto;
            txtNombre.setText(contacto.getNombre());
            txtTelefono1.setText(contacto.getTelefono1());
            txtTelefono2.setText(contacto.getTelefono2());
            txtDomicilio.setText(contacto.getDomicilio());
            txtNotas.setText(contacto.getNotas());
            checkBoxFav.setChecked(contacto.getFavorito()>0);
        }else{
            limpiar();
        }
    }
}
